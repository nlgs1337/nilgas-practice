self.addEventListener('install', function(event){
    console.log('[Service Worker] Installing Service Worker ...', event);
    event.waitUntil(
        caches.open("static")
        .then(function(cache){
            console.log("precaching");
            cache.add('/index.html');
            cache.add('/css/style.css');
            cache.add('/css/responsive.css');
            cache.add('//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js');
            cache.add('//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css');
            cache.add('//use.fontawesome.com/releases/v5.0.8/js/all.js');
            cache.add('/img/logo.png');
            cache.add('/img/wheat.png');
            cache.add('/img/watermelon.png');
            cache.add('/img/clover.png');
            cache.add('/img/sunfl.png');
            cache.add('/img/tomato.png');
            cache.add('/img/eggplant.png');
            cache.add('/img/or_sunfl.png');
            cache.add('/img/image 3.jpg');
            cache.add('/img/image 2.jpg');
            cache.add('/img/image 4.png');
            cache.add('/img/image 5.png');
            cache.add('/img/image 6.png');
            cache.add('/');
        })
    );
});
self.addEventListener('activate', function(event){
    console.log('[Service Worker] Activating Service Worker ...', event);
    return self.clients.claim();
});
self.addEventListener('fetch',function(event){
    event.respondWith(
        caches.match(event.request)
            .then(function(response){
                if (response)
                    return response;
                else
                    return fetch(event.request);
            }
        )
    );
});
self.addEventListener('push',event =>{
    const notification = event.data.text();
    self.registration.showNotification(notification, {});
});