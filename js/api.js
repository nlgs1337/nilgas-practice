    fetch('https://restcountries.eu/rest/v2/regionalbloc/eu')
    .then(res => res.json())
    .then(data => {
        const Capitals = document.createElement('ul');
        Capitals.classList.add("category-items");
        data.forEach(function(item){
            const viewItem=document.createElement('li');
            viewItem.appendChild(document.createTextNode(item.capital));
            Capitals.appendChild(viewItem);            
        });
        document.getElementsByClassName("api")[0].getElementsByClassName("result")[0].appendChild(Capitals);     
        
    });

    